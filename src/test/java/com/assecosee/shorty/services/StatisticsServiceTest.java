package com.assecosee.shorty.services;

import com.assecosee.shorty.dtos.shorturl.ShortUrlStatisticsDTO;
import com.assecosee.shorty.exceptions.ShortyException;
import com.assecosee.shorty.models.ShortUrl;
import com.assecosee.shorty.models.User;
import com.assecosee.shorty.repositories.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThrows;

@RunWith(MockitoJUnitRunner.class)
public class StatisticsServiceTest {

    @InjectMocks
    private StatisticsService statisticsService;

    @Mock
    private UserRepository userRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @Before
    public void setUp() {
        User existing = new User();
        existing.setAccountId("user");
        existing.setPassword(bCryptPasswordEncoder.encode("pass"));

        Mockito.when(userRepository.findByAccountId(existing.getAccountId()))
                .thenReturn(Optional.of(existing));

        ShortUrl existingUrl = new ShortUrl();
        existingUrl.setOriginalUrl("https://www.baeldung.com/spring-boot-testing");
        existingUrl.setShortenedUrl("short");
        existingUrl.setRedirectType(301);
        existingUrl.setUser(existing);

        existing.setUrls(Collections.singletonList(existingUrl));

        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    @Test
    public void should_get_statistics_when_user_is_authenticated(){
        Mockito.when(SecurityContextHolder.getContext().getAuthentication().getName()).thenReturn("user");

        List<ShortUrlStatisticsDTO> shortUrlStatisticsDTOS = statisticsService.getStatistics();

        assertThat(shortUrlStatisticsDTOS, notNullValue());
    }

    @Test
    public void should_fail_get_statistics_when_user_is_not_authenticated(){
        Mockito.when(SecurityContextHolder.getContext().getAuthentication().getName()).thenReturn("non-existing");
        assertThrows(ShortyException.class, () -> statisticsService.getStatistics());
    }
}
