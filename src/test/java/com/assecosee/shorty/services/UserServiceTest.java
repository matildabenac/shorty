package com.assecosee.shorty.services;

import com.assecosee.shorty.dtos.user.RegisterUserDTO;
import com.assecosee.shorty.dtos.user.UpdateUserPasswordDTO;
import com.assecosee.shorty.dtos.user.UserDTO;
import com.assecosee.shorty.exceptions.ShortyException;
import com.assecosee.shorty.helpers.StringHelper;
import com.assecosee.shorty.models.User;
import com.assecosee.shorty.repositories.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThrows;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private StringHelper stringHelper;

    //ovo isto možeš mockat
    private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @Before
    public void setUp() {
        User existing = new User();
        existing.setAccountId("existing");
        existing.setPassword(bCryptPasswordEncoder.encode("mockPassword"));

        Mockito.when(userRepository.findByAccountId(existing.getAccountId()))
                .thenReturn(Optional.of(existing));

        Mockito.when(stringHelper.randomStringGenerator(16)).thenReturn("1234567890abcdef");
    }

    @Test
    public void should_register_user_when_account_id_is_valid_and_doesnt_exist(){
        RegisterUserDTO registerUserDTO = new RegisterUserDTO();
        registerUserDTO.setAccountId("Valid");

        UserDTO userDTO = userService.registerUser(registerUserDTO);

        assertThat(userDTO, notNullValue());
    }

    @Test
    public void should_fail_register_user_when_account_id_exists(){
        RegisterUserDTO registerUserDTO = new RegisterUserDTO();
        registerUserDTO.setAccountId("existing");

        assertThrows(ShortyException.class, () -> {
            userService.registerUser(registerUserDTO);
        });
    }

    @Test
    public void should_fail_register_user_when_account_id_is_invalid(){
        RegisterUserDTO registerUserDTO = new RegisterUserDTO();
        registerUserDTO.setAccountId("not valid $");

        UserDTO userDTO = userService.registerUser(registerUserDTO);

        assertThat(userDTO, notNullValue());
    }


    @Test
    public void should_update_user_password_when_user_exists_and_old_password_is_correct_and_newPassword_is_valid(){
        Mockito.when(stringHelper.checkStringValidity("newPassword")).thenReturn(true);

        String accountId = "existing";

        UpdateUserPasswordDTO updateUserPasswordDTO = new UpdateUserPasswordDTO();
        updateUserPasswordDTO.setOldPassword("mockPassword");
        updateUserPasswordDTO.setNewPassword("newPassword");

        UserDTO userDTO = userService.updateUserPassword(accountId, updateUserPasswordDTO);

        assertThat(userDTO, notNullValue());
    }

    @Test
    public void should_fail_update_user_when_new_password_is_invalid(){
        String accountId = "existing";
        UpdateUserPasswordDTO updateUserPasswordDTO = new UpdateUserPasswordDTO();
        updateUserPasswordDTO.setOldPassword("mockPassword");
        updateUserPasswordDTO.setNewPassword("i n v a l i d <> p a s s w o r d");

        assertThrows(ShortyException.class, () -> {
            userService.updateUserPassword(accountId, updateUserPasswordDTO);
        });
    }

    @Test
    public void should_fail_update_user_when_user_doesnt_exist(){
        String accountId = "notExisting";
        UpdateUserPasswordDTO updateUserPasswordDTO = new UpdateUserPasswordDTO();
        updateUserPasswordDTO.setOldPassword("mockPassword");
        updateUserPasswordDTO.setNewPassword("newPassword");

        assertThrows(ShortyException.class, () -> {
            userService.updateUserPassword(accountId, updateUserPasswordDTO);
        });
    }

    @Test
    public void should_fail_update_user_when_old_password_is_incorrect(){
        String accountId = "existing";
        UpdateUserPasswordDTO updateUserPasswordDTO = new UpdateUserPasswordDTO();
        updateUserPasswordDTO.setOldPassword("incorrectPassword");
        updateUserPasswordDTO.setNewPassword("newPassword");

        assertThrows(ShortyException.class, () -> {
            userService.updateUserPassword(accountId, updateUserPasswordDTO);
        });
    }
}
