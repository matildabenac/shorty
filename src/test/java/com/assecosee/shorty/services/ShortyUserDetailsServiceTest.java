package com.assecosee.shorty.services;

import com.assecosee.shorty.models.User;
import com.assecosee.shorty.repositories.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThrows;


@RunWith(MockitoJUnitRunner.class)
public class ShortyUserDetailsServiceTest {

    @InjectMocks
    private ShortyUserDetailsService shortyUserDetailsService;

    @Mock
    private UserRepository userRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @Before
    public void setUp() {
        User existing = new User();
        existing.setAccountId("existing");
        existing.setPassword(bCryptPasswordEncoder.encode("mockPassword"));

        Mockito.when(userRepository.findByAccountId(existing.getAccountId()))
                .thenReturn(java.util.Optional.of(existing));
    }

    @Test
    public void should_load_user_by_username_when_username_exists(){
        String username = "existing";

        UserDetails userDetails = shortyUserDetailsService.loadUserByUsername(username);

        assertThat(userDetails, notNullValue());
    }

    @Test
    public void should_fail_load_user_by_username_when_username_doesnt_exist(){
        String username = "non-existing";

        assertThrows(UsernameNotFoundException.class, () -> shortyUserDetailsService.loadUserByUsername(username));
    }
}
