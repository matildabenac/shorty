package com.assecosee.shorty.services;

import com.assecosee.shorty.dtos.shorturl.ShortUrlDTO;
import com.assecosee.shorty.exceptions.ShortyException;
import com.assecosee.shorty.helpers.UrlHelper;
import com.assecosee.shorty.models.ShortUrl;
import com.assecosee.shorty.models.User;
import com.assecosee.shorty.repositories.ShortUrlRepository;
import com.assecosee.shorty.repositories.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class ShortUrlServiceTest {

    @InjectMocks
    private ShortUrlService shortUrlService;

    @Mock
    private ShortUrlRepository shortUrlRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UrlHelper urlHelper;

    private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @Before
    public void setUp() {
        User existing = new User();
        existing.setAccountId("user");
        existing.setPassword(bCryptPasswordEncoder.encode("pass"));

        Mockito.when(userRepository.findByAccountId(existing.getAccountId()))
                .thenReturn(Optional.of(existing));

        ShortUrl existingUrl = new ShortUrl();
        existingUrl.setOriginalUrl("https://www.baeldung.com/spring-boot-testing");
        existingUrl.setShortenedUrl("short");
        existingUrl.setRedirectType(301);

        Mockito.when(shortUrlRepository.findByOriginalUrl(existingUrl.getOriginalUrl()))
                .thenReturn(Optional.of(existingUrl));
        Mockito.when(shortUrlRepository.findByShortenedUrl(existingUrl.getShortenedUrl()))
                .thenReturn(Optional.of(existingUrl));

        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        Mockito.when(SecurityContextHolder.getContext().getAuthentication().getName()).thenReturn("user");
    }

    @Test
    public void should_shorten_url_when_original_url_and_redirect_type_are_valid(){
        String url = "https://images-na.ssl-images-amazon.com/images/I/61pPNsV6bgL._AC_SL1200_.jpg";
        Mockito.when(urlHelper.isUrlValid(url)).thenReturn(true);

        ShortUrlDTO shortUrlRequestDTO = createShortUrlDTO(url, 302);

        ShortUrlDTO shortUrlResponseDTO = shortUrlService.shortenUrl(shortUrlRequestDTO);

        assertThat(shortUrlResponseDTO, notNullValue());
    }

    @Test
    public void should_fail_shorten_url_when_original_url_is_invalid(){
        String url = "invalid url";
        Mockito.when(urlHelper.isUrlValid(url)).thenReturn(false);
        ShortUrlDTO shortUrlRequestDTO = createShortUrlDTO(url, 301);

        assertThrows(ShortyException.class, () -> shortUrlService.shortenUrl(shortUrlRequestDTO));
    }

    @Test
    public void should_return_shortened_url_when_original_url_exists(){
        String url = "https://www.baeldung.com/spring-boot-testing";
        Mockito.when(urlHelper.isUrlValid(url)).thenReturn(true);
        ShortUrlDTO shortUrlRequestDTO = createShortUrlDTO(url, 301);

        Optional<ShortUrl> shortUrl = shortUrlRepository.findByOriginalUrl(shortUrlRequestDTO.getUrl());
        assertTrue(shortUrl.isPresent());

        Mockito.when(urlHelper.getFullShortUrl(shortUrl.get().getShortenedUrl())).thenReturn("http://localhost:8080/short/short");

        ShortUrlDTO shortUrlResponseDTO = shortUrlService.shortenUrl(shortUrlRequestDTO);

        assertThat(shortUrlResponseDTO, notNullValue());
        assertThat(urlHelper.getFullShortUrl(shortUrl.get().getShortenedUrl()), equalTo(shortUrlResponseDTO.getUrl()));
    }

    @Test
    public void should_fail_shorten_url_when_redirect_type_is_invalid(){
        String url = "https://images-na.ssl-images-amazon.com/images/I/61pPNsV6bgL._AC_SL1200_.jpg";
        Mockito.when(urlHelper.isUrlValid(url)).thenReturn(true);
        ShortUrlDTO shortUrlRequestDTO = createShortUrlDTO(url, 304);

        assertThrows(ShortyException.class, () -> shortUrlService.shortenUrl(shortUrlRequestDTO));
    }

    @Test
    public void should_redirect_shortened_url_to_original_url_when_short_url_exists(){
        String url = "short";

        ShortUrl shortUrl = shortUrlService.redirectToUrl(url);

        assertTrue(shortUrl.getRedirectType() == 301 || shortUrl.getRedirectType() == 302);
    }

    @Test
    public void should_fail_redirect_shortened_url_to_original_url_when_short_url_doesnt_exist(){
        String url = "non-existing";

        assertThrows(ShortyException.class, () -> shortUrlService.redirectToUrl(url));
    }


    private ShortUrlDTO createShortUrlDTO(String url, int redirectType){
        ShortUrlDTO shortUrlDTO = new ShortUrlDTO();
        shortUrlDTO.setUrl(url);
        shortUrlDTO.setRedirectType(redirectType);

        return shortUrlDTO;
    }
}
