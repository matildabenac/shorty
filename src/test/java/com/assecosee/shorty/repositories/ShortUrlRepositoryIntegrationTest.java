package com.assecosee.shorty.repositories;

import com.assecosee.shorty.models.ShortUrl;
import com.assecosee.shorty.models.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ShortUrlRepositoryIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ShortUrlRepository shortUrlRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @Before
    public void setUp(){
        User user = new User();
        user.setAccountId("existing");
        user.setPassword(bCryptPasswordEncoder.encode("mockPass"));

        entityManager.persist(user);
        entityManager.flush();

        ShortUrl shortUrl = new ShortUrl();
        shortUrl.setOriginalUrl("https://images-na.ssl-images-amazon.com/images/I/61pPNsV6bgL._AC_SL1200_.jpg");
        shortUrl.setShortenedUrl("short");
        shortUrl.setRedirectType(301);
        shortUrl.setUser(user);

        entityManager.persist(shortUrl);
        entityManager.flush();
    }

    @Test
    public void Should_find_url_by_shortened_url_when_shortened_url_exists(){
        Optional<ShortUrl> found = shortUrlRepository.findByShortenedUrl("short");

        assertTrue(found.isPresent());
    }

    @Test
    public void Should_fail_find_url_by_shortened_url_when_shortened_url_doesnt_exist(){
        Optional<ShortUrl> found = shortUrlRepository.findByShortenedUrl("non-existing");

        assertFalse(found.isPresent());
    }

    @Test
    public void Should_find_url_by_original_url_when_original_url_exists(){
        Optional<ShortUrl> found = shortUrlRepository.findByOriginalUrl("https://images-na.ssl-images-amazon.com/images/I/61pPNsV6bgL._AC_SL1200_.jpg");

        assertTrue(found.isPresent());
    }

    @Test
    public void Should_fail_find_url_by_original_url_when_original_url_doesnt_exists(){
        Optional<ShortUrl> found = shortUrlRepository.findByOriginalUrl("https://non.existing");

        assertFalse(found.isPresent());
    }
}
