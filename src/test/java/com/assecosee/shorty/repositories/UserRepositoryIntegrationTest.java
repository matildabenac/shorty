package com.assecosee.shorty.repositories;

import com.assecosee.shorty.models.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @Before
    public void setUp(){
        User user = new User();
        user.setAccountId("existing");
        user.setPassword(bCryptPasswordEncoder.encode("mockPass"));

        entityManager.persist(user);
        entityManager.flush();
    }

    @Test
    public void Should_find_user_by_accountId_when_accountId_exists(){
        Optional<User> found = userRepository.findByAccountId("existing");

        assertTrue(found.isPresent());
    }

    @Test
    public void Should_fail_find_user_by_accountId_when_accountId_doesnt_exists(){
        Optional<User> found = userRepository.findByAccountId("non-existing");

        assertFalse(found.isPresent());
    }
}
