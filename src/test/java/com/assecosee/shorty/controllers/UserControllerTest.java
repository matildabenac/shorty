package com.assecosee.shorty.controllers;

import com.assecosee.shorty.dtos.user.RegisterUserDTO;
import com.assecosee.shorty.dtos.user.UpdateUserPasswordDTO;
import com.assecosee.shorty.dtos.user.UserDTO;
import com.assecosee.shorty.exceptions.ShortyException;
import com.assecosee.shorty.models.User;
import com.assecosee.shorty.services.ShortyUserDetailsService;
import com.assecosee.shorty.services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class) // SpringRunner.class
@WebMvcTest(UserController.class)
//@ComponentScan("com.assecosee.shorty")
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private ShortyUserDetailsService shortyUserDetailsService;

    private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @Autowired
    private ObjectMapper objectMapper;

    @Before
    public void setUp(){
        User existingUser = new User();
        existingUser.setAccountId("existing");
        existingUser.setPassword(bCryptPasswordEncoder.encode("mockPass"));

        UserDTO userDTO = new UserDTO("newUser", "mockPass");

        RegisterUserDTO registerUserDTO = new RegisterUserDTO();
        registerUserDTO.setAccountId("newUser");
        Mockito.when(userService.registerUser(registerUserDTO))
                .thenReturn(userDTO);
    }


    @Test
    public void should_register_user_when_account_id_is_valid() throws Exception {
        RegisterUserDTO registerUserDTO = new RegisterUserDTO();
        registerUserDTO.setAccountId("newUser");

        UserDTO userDTO = new UserDTO("newUser", "mockPass");

        mockMvc.perform(post("/administration/register")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(registerUserDTO))
                )
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(userDTO)));
    }

    @Test
    public void should_return_500_when_account_id_is_invalid() throws Exception {
        final RegisterUserDTO registerUserDTO = new RegisterUserDTO();
        registerUserDTO.setAccountId("invalid user");

        Mockito.when(userService.registerUser(registerUserDTO))
                .thenThrow(new ShortyException(HttpStatus.INTERNAL_SERVER_ERROR, "Account Id contains forbidden characters"));

        //operate & check
        mockMvc.perform(post("/administration/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(registerUserDTO))
        )
                .andExpect(status().isInternalServerError());
    }

    @Test
    public void should_return_400_when_trying_to_register_invalid_user() throws Exception {
        //operate & check
        mockMvc.perform(post("/administration/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(null))
        )
                .andExpect(status().isBadRequest());
    }


    @Test
    public void should_return_404_for_existing_user() throws Exception {
        ///build
        final RegisterUserDTO registerUserDTO = new RegisterUserDTO();
        registerUserDTO.setAccountId("newUser");
        Mockito.when(userService.registerUser(registerUserDTO))
                .thenThrow(new ShortyException(HttpStatus.CONFLICT, "User with that account ID is already registered!"));

        //operate & check
        mockMvc.perform(post("/administration/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(registerUserDTO))
        )
                .andExpect(status().isConflict());
    }


    @Test
    @WithMockUser(username="existing", password="mockPass")
    public void should_update_user_when_accound_id_exists_old_password_is_correct_new_password_is_valid() throws Exception {
        UpdateUserPasswordDTO updateUserPasswordDTO = new UpdateUserPasswordDTO();
        updateUserPasswordDTO.setOldPassword("mockPass");
        updateUserPasswordDTO.setNewPassword("validPassword");

        UserDTO userDTO = new UserDTO("existing", "validPassword");

        mockMvc.perform(put("/administration/update/existing")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateUserPasswordDTO))
        )
                .andExpect(status().isOk());
//                .andExpect(content().json(objectMapper.writeValueAsString(userDTO))); //Unparsable JSON string
    }
}
