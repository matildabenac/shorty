package com.assecosee.shorty.services;

import com.assecosee.shorty.authdetails.ShortyUserDetails;
import com.assecosee.shorty.models.User;
import com.assecosee.shorty.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class ShortyUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public ShortyUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        final Optional<User> user = userRepository.findByAccountId(s);

        if(! user.isPresent()){
            log.error("User with account ID '{}' does not exist", s);
            throw new UsernameNotFoundException("User with account ID '" + s + "' does not exist");
        }

        log.info("Current user, {}", user.get());
        return new ShortyUserDetails(user.get());
    }
}
