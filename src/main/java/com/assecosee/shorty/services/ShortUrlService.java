package com.assecosee.shorty.services;

import com.assecosee.shorty.dtos.shorturl.ShortUrlDTO;
import com.assecosee.shorty.exceptions.ShortyException;
import com.assecosee.shorty.helpers.UrlHelper;
import com.assecosee.shorty.models.ShortUrl;
import com.assecosee.shorty.models.User;
import com.assecosee.shorty.repositories.ShortUrlRepository;
import com.assecosee.shorty.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class ShortUrlService {

    private final ShortUrlRepository shortUrlRepository;
    private final UserRepository userRepository;
    private final UrlHelper urlHelper;

    @Autowired
    public ShortUrlService(ShortUrlRepository shortUrlRepository, UserRepository userRepository, UrlHelper urlHelper) {
        this.shortUrlRepository = shortUrlRepository;
        this.userRepository = userRepository;
        this.urlHelper = urlHelper;
    }

    public ShortUrlDTO shortenUrl(ShortUrlDTO urlToShorten)
    {
        final String url = urlToShorten.getUrl();
        final int redirectType = urlToShorten.getRedirectType();

        log.info(url, redirectType);

        checkUrlValidity(url);
        checkRedirectType(redirectType);

        ShortUrlDTO shortUrlResponseDTO = getExistingUrl(url);
        if(shortUrlResponseDTO != null) return shortUrlResponseDTO;

        final ShortUrl newShortUrl = createNewShortUrl(url, redirectType);

        shortUrlRepository.save(newShortUrl);

        shortUrlResponseDTO = new ShortUrlDTO(newShortUrl);
        log.info(shortUrlResponseDTO.toString());

        return shortUrlResponseDTO;
    }

    public ShortUrl redirectToUrl(String shortUrl){
        final Optional<ShortUrl> optionalUrl = shortUrlRepository.findByShortenedUrl(shortUrl);
        if(! optionalUrl.isPresent()){
            log.error("Url '{}' does not exist", urlHelper.getFullShortUrl(shortUrl));
            throw new ShortyException(HttpStatus.NOT_FOUND, "Url '" + urlHelper.getFullShortUrl(shortUrl) + "' does not exist");
        }

        final ShortUrl url = optionalUrl.get();

        int statusCode = url.getRedirectType();
        try{
            checkRedirectType(statusCode);
        } catch (ShortyException e){
            log.warn("Url redirect type was: {} - setting redirect type to 302", statusCode);
            url.setRedirectType(302);
        }

        updateNumberOfRedirects(url);

        shortUrlRepository.save(url);

        log.info("Redirect of type '{}' to '{}'", statusCode, url.getOriginalUrl());
        return url;
    }


    private void checkUrlValidity(String url){
        if(!urlHelper.isUrlValid(url)){
            log.error("The url '{}' is invalid", url);
            throw new ShortyException(HttpStatus.BAD_REQUEST, "The url '" + url + "' is invalid");
        }
    }

    private void checkRedirectType(int redirectType){
        if(redirectType != 301 && redirectType != 302){
            log.error("Redirect type expects value of '301' or '302', but has value '{}'", redirectType);
            throw new ShortyException(HttpStatus.BAD_REQUEST, "Redirect type expects value of '301' or '302', but has value '" + redirectType + "'");
        }
    }

    private ShortUrlDTO getExistingUrl(String url){
        final Optional<ShortUrl> existingUrl = shortUrlRepository.findByOriginalUrl(url);
        if(existingUrl.isPresent()){
            log.warn("Url '{}' already exists, short url is '{}'", url, urlHelper.getFullShortUrl(existingUrl.get().getShortenedUrl()));
            return new ShortUrlDTO(existingUrl.get());
        }
        return null;
    }

    private User getCurrentUser(){
        final String accountId = SecurityContextHolder.getContext().getAuthentication().getName();
        final Optional<User> currentUser = userRepository.findByAccountId(accountId);
        if(!currentUser.isPresent()){
            log.error("No user is authenticated");
            throw new ShortyException(HttpStatus.UNAUTHORIZED, "No user is authenticated");
        }
        log.info(currentUser.get().toString());
        return currentUser.get();
    }

    private String getShortUrl(){
        int stringLen = 8;
        String shortUrl;
        Optional<ShortUrl> existingUrl;

        do{
            shortUrl = urlHelper.shortUrlGenerator(stringLen);
            existingUrl = shortUrlRepository.findByShortenedUrl(shortUrl);
            stringLen++;
        } while (existingUrl.isPresent());

        log.info(shortUrl);
        return shortUrl;
    }

    private ShortUrl createNewShortUrl(String url, int redirectType){
        ShortUrl newShortUrl = new ShortUrl();
        newShortUrl.setOriginalUrl(url);
        newShortUrl.setShortenedUrl(getShortUrl());
        newShortUrl.setRedirectType(redirectType);
        newShortUrl.setUser(getCurrentUser());

        log.info(newShortUrl.toString());

        return newShortUrl;
    }

    private void updateNumberOfRedirects(ShortUrl url){
        int numberOfRedirects = url.getNumberOfRedirects() + 1;
        url.setNumberOfRedirects(numberOfRedirects);
    }

}
