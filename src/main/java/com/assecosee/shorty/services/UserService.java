package com.assecosee.shorty.services;

import com.assecosee.shorty.dtos.user.RegisterUserDTO;
import com.assecosee.shorty.dtos.user.UserDTO;
import com.assecosee.shorty.dtos.user.UpdateUserPasswordDTO;
import com.assecosee.shorty.exceptions.ShortyException;
import com.assecosee.shorty.helpers.StringHelper;
import com.assecosee.shorty.models.User;
import com.assecosee.shorty.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Service
public class UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
    private final StringHelper stringHelper;

    @Autowired
    public UserService(UserRepository userRepository, StringHelper stringHelper) {
        this.userRepository = userRepository;
        this.stringHelper = stringHelper;
    }

    public UserDTO registerUser(RegisterUserDTO user) {

        log.info(user.toString());
        String accountId = user.getAccountId();

        final String password = stringHelper.randomStringGenerator(16);
        final String encodedPassword = bCryptPasswordEncoder.encode(password);

        log.info("Password: {}", password);
        log.info("Encoded password: {}", encodedPassword);

        User newUser = new User();
        newUser.setAccountId(accountId);
        newUser.setPassword(encodedPassword);

        validateUser(newUser);

        checkUserRegistered(accountId);

        userRepository.saveAndFlush(newUser);

        log.info(newUser.toString());
        return new UserDTO(accountId, password);
    }

    public UserDTO updateUserPassword(String accountId, UpdateUserPasswordDTO passwords){

        log.info(passwords.toString());

        checkStringAlphanumeric(passwords.getNewPassword());

        User userToUpdate = getUserToUpdate(accountId);
        log.info(userToUpdate.toString());

        verifyPassword(passwords.getOldPassword(), userToUpdate.getPassword());

        // save user with new password
        userToUpdate.setPassword(bCryptPasswordEncoder.encode(passwords.getNewPassword()));
        userRepository.save(userToUpdate);

        log.info("Old password: {}", passwords.getOldPassword());
        log.info("New password: {}, encoded new password: {}", passwords.getNewPassword(), userToUpdate.getPassword());

        log.info(userToUpdate.toString());
        return new UserDTO(accountId, passwords.getNewPassword());
    }


    private void checkUserRegistered(String accountId){
        if(userRepository.findByAccountId(accountId).isPresent()) {
            log.error("User with account ID '{}' is already registered!", accountId);
            throw new ShortyException(HttpStatus.CONFLICT, "User with account ID '" + accountId + "' is already registered!");
        }
    }

    private void validateUser(User newUser){
        final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        final Validator validator = factory.getValidator();

        final Set<ConstraintViolation<User>> constraintViolations = validator.validate(newUser);

        if(!constraintViolations.isEmpty()){
            for(ConstraintViolation<User> violation: constraintViolations) {
                log.error(violation.getMessage());
            }
        }
    }

    private void checkStringAlphanumeric(String string){
        if(! stringHelper.checkStringValidity(string)){
            log.error("Password '{}' contains forbidden characters - only alphanumeric values are valid", string);
            throw new ShortyException(HttpStatus.BAD_REQUEST, "Password '" + string + "' contains forbidden characters - only alphanumeric values are valid");
        }
    }

    private void verifyPassword(String oldPassword, String userPassword){
        if(!bCryptPasswordEncoder.matches(oldPassword, userPassword)){
            log.error("Incorrect old password '" + oldPassword + "'");
            throw new ShortyException(HttpStatus.UNAUTHORIZED, "Incorrect old password '" + oldPassword + "'");
        }
    }

    private User getUserToUpdate(String accountId){
        final Optional<User> userToUpdate = userRepository.findByAccountId(accountId);

        if(!userToUpdate.isPresent()){
            log.error("User with account ID '" + accountId + "' doesn't exist");
            throw new ShortyException(HttpStatus.NOT_FOUND, "User with account ID '" + accountId + "' doesn't exist");
        }

        return userToUpdate.get();
    }

}
