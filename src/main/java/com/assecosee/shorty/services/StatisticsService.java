package com.assecosee.shorty.services;

import com.assecosee.shorty.dtos.shorturl.ShortUrlStatisticsDTO;
import com.assecosee.shorty.exceptions.ShortyException;
import com.assecosee.shorty.models.ShortUrl;
import com.assecosee.shorty.models.User;
import com.assecosee.shorty.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Slf4j
@Service
public class StatisticsService {

    private final UserRepository userRepository;

    @Autowired
    public StatisticsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<ShortUrlStatisticsDTO> getStatistics()
    {
        final String accountId = SecurityContextHolder.getContext().getAuthentication().getName();
        log.info("Getting user account '{}'", accountId);

        Optional<User> user = userRepository.findByAccountId(accountId);

        if(! user.isPresent()){
            log.error("User with account ID '{}' doesn't exist", accountId);
            throw new ShortyException(HttpStatus.UNAUTHORIZED, "User with account ID '" + accountId + "' doesn't exist");
        }

        log.info(user.get().toString());

        log.info(user.get().getUrls().toString());
        return getShortUrlStatisticsDTOs(user.get().getUrls());
    }

    private List<ShortUrlStatisticsDTO> getShortUrlStatisticsDTOs(List<ShortUrl> urls){
        List<ShortUrlStatisticsDTO> shortUrlStatisticsDTOS = urls.stream().map(ShortUrlStatisticsDTO::new).collect(Collectors.toList());

        log.info(shortUrlStatisticsDTOS.toString());
        return shortUrlStatisticsDTOS;
    }

}
