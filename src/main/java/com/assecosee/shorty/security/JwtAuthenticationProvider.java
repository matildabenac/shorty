//package com.assecosee.shorty.security;
//
//import com.assecosee.shorty.models.User;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.AuthorityUtils;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.oauth2.jwt.Jwt;
//import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
//
//import java.util.List;
//
//public class JwtAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
//    @Autowired
//    private JwtUtil jwtUtil;
//
//    @Override
//    public boolean supports(Class<?> authentication) {
//        return (JwtAuthenticationToken.class.isAssignableFrom(authentication));
//    }
//
//    @Override
//    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
//    }
//
//    @Override
//    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
//        JwtAuthenticationToken jwtAuthenticationToken = authentication.getCredentials();
//        Jwt token = jwtAuthenticationToken.getToken();
//
//        User parsedUser = jwtUtil.parseToken(token.getTokenValue());
//
//        if (parsedUser == null) {
////            throw new JwtTokenMalformedException("JWT token is not valid");
//        }
//
////        List<GrantedAuthority> authorityList = AuthorityUtils.commaSeparatedStringToAuthorityList(parsedUser.getRole());
//
//        return new org.springframework.security.core.userdetails.User(parsedUser.getId(), parsedUser.getAccountId(), token);
//    }
//}
