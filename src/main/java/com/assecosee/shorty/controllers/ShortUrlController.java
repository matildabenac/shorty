package com.assecosee.shorty.controllers;

import com.assecosee.shorty.dtos.shorturl.ShortUrlDTO;
import com.assecosee.shorty.exceptions.ShortyException;
import com.assecosee.shorty.models.ShortUrl;
import com.assecosee.shorty.services.ShortUrlService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

import static com.assecosee.shorty.helpers.constants.ShortUrlControllerConstants.*;

@Slf4j
@RestController
@RequestMapping(path = SHORT_URL_CONTROLLER_PATH)
public class ShortUrlController {

    private final ShortUrlService shortUrlService;


    @Autowired
    public ShortUrlController(ShortUrlService shortenerService) {
        this.shortUrlService = shortenerService;
    }


    @PostMapping(path = SHORTEN_URL_PATH)
    public ResponseEntity<ShortUrlDTO> shortenUrl(@RequestBody ShortUrlDTO url)
    {
        log.info(url.toString());
        return ResponseEntity.ok(shortUrlService.shortenUrl(url));
    }

    @GetMapping(path = REDIRECT_TO_URL)
    public ResponseEntity<Object> redirectToUrl(@PathVariable String shortUrl)
    {
        log.info(shortUrl);

        ShortUrl redirectUrl = shortUrlService.redirectToUrl(shortUrl);

        HttpStatus redirect = HttpStatus.resolve(redirectUrl.getRedirectType());

        if(redirect == null){
            throw new ShortyException(HttpStatus.BAD_REQUEST, "Cannot resolve redirect to '" + redirectUrl.getOriginalUrl() + "'");
        }

        return ResponseEntity.status(redirect).location(URI.create(redirectUrl.getOriginalUrl())).build();
    }
}
