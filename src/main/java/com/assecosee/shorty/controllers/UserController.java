package com.assecosee.shorty.controllers;

import com.assecosee.shorty.dtos.user.RegisterUserDTO;
import com.assecosee.shorty.dtos.user.UpdateUserPasswordDTO;
import com.assecosee.shorty.dtos.user.UserDTO;
import com.assecosee.shorty.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.assecosee.shorty.helpers.constants.UserControllerConstants.*;


@Slf4j
@RestController
@RequestMapping(path = USER_CONTROLLER_PATH)
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(path = REGISTER_USER_PATH)
    public ResponseEntity<UserDTO> registerUser(@RequestBody RegisterUserDTO user) {
        log.info(user.toString());
        return ResponseEntity.ok(userService.registerUser(user));
    }

    @PutMapping(path = UPDATE_USER_PASSWORD_PATH)
    public ResponseEntity<UserDTO> updateUserPassword(@PathVariable String accountId, @RequestBody UpdateUserPasswordDTO passwords){
        log.info(accountId, passwords.toString());
        return ResponseEntity.ok(userService.updateUserPassword(accountId, passwords));
    }
}
