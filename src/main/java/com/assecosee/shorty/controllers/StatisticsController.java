package com.assecosee.shorty.controllers;

import com.assecosee.shorty.dtos.shorturl.ShortUrlStatisticsDTO;
import com.assecosee.shorty.services.StatisticsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.assecosee.shorty.helpers.constants.StatisticsControllerConstants.GET_STATISTICS_PATH;


@Slf4j
@RestController
public class StatisticsController {

    private final StatisticsService statisticsService;

    @Autowired
    public StatisticsController(StatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    @GetMapping(path = GET_STATISTICS_PATH)
    public ResponseEntity<List<ShortUrlStatisticsDTO>> getStatistics()
    {
        return ResponseEntity.ok(statisticsService.getStatistics());
    }
}
