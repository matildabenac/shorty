package com.assecosee.shorty.repositories;

import com.assecosee.shorty.models.ShortUrl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ShortUrlRepository extends JpaRepository<ShortUrl, Long> {

    Optional<ShortUrl> findByShortenedUrl(String shortUrl);

    Optional<ShortUrl> findByOriginalUrl(String originalUrl);

}
