package com.assecosee.shorty.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.assecosee.shorty.helpers.constants.ModelSequenceConstants.SHORT_URL_SEQUENCE;

@Entity
@Table(
        name = "short_url",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"originalUrl"}),
                @UniqueConstraint(columnNames = {"shortenedUrl"}),
        }
)
@Data
public class ShortUrl {

    @Id
    @SequenceGenerator(
            name= SHORT_URL_SEQUENCE,
            sequenceName= SHORT_URL_SEQUENCE,
            allocationSize = 1
    )
    @GeneratedValue(
            strategy=GenerationType.SEQUENCE,
            generator = SHORT_URL_SEQUENCE
    )
    private Long id;

    @Column
    @NotNull(message = "{originalUrl.notnull}")
    @Size(max = 255, message = "{originalUrl.size}")
    private String originalUrl;
    @Column
    @NotNull(message = "{shortenedUrl.notnull}")
    @Size(max = 255, message = "{shortenedUrl.size}")
    private String shortenedUrl;
    @Column
    @NotNull(message = "{redirectType.notnull}")
    private Integer redirectType;
    @Column
    private int numberOfRedirects = 0;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "userId", nullable = false)
    @JsonIgnore
    private User user;

    @Override
    public String toString() {
        return "ShortUrl{" +
                "id=" + id +
                ", originalUrl='" + originalUrl + '\'' +
                ", shortenedUrl='" + shortenedUrl + '\'' +
                ", redirectType=" + redirectType +
                ", numberOfRedirects=" + numberOfRedirects +
                '}';
    }
}
