package com.assecosee.shorty.models;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

import static com.assecosee.shorty.helpers.constants.ModelSequenceConstants.USER_SEQUENCE;

@Entity
@Table(
        name = "user",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"accountId"})
        }
)
@Data
public class User {
    @Id
    @SequenceGenerator(
            name= USER_SEQUENCE,
            sequenceName= USER_SEQUENCE,
            allocationSize = 1
    )
    @GeneratedValue(
            strategy=GenerationType.SEQUENCE,
            generator = USER_SEQUENCE
    )
    private Long id;

    @Column
    @NotNull(message = "{accountId.notnull}")
    @Size(max = 255, message = "{accountId.size}")
    @Pattern(regexp="[A-Za-z0-9]+", message = "{accountId.pattern}")
    private String accountId;
    @Column
    @NotNull(message = "{password.notnull}")
    @Size(max = 255, message = "{password.size}")
    private String password;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<ShortUrl> urls;
}

