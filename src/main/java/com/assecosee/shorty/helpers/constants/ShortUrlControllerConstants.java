package com.assecosee.shorty.helpers.constants;

public class ShortUrlControllerConstants {
    public static final String SHORT_URL_CONTROLLER_PATH = "/short";
    public static final String SHORTEN_URL_PATH = "/shortener";
    public static final String REDIRECT_TO_URL = "/{shortUrl}";
}
