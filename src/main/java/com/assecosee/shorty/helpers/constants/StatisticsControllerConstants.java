package com.assecosee.shorty.helpers.constants;

public class StatisticsControllerConstants {
    public static final String GET_STATISTICS_PATH = "/statistics/fetch";
}
