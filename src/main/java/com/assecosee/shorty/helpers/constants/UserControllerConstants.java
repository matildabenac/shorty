package com.assecosee.shorty.helpers.constants;

public class UserControllerConstants {
    public static final String USER_CONTROLLER_PATH = "/administration";
    public static final String REGISTER_USER_PATH = "/register";
    public static final String UPDATE_USER_PASSWORD_PATH = "/update/{accountId}";
}
