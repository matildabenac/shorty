package com.assecosee.shorty.helpers.constants;

public class ModelSequenceConstants {
    public static final String USER_SEQUENCE = "user_sequence";
    public static final String SHORT_URL_SEQUENCE = "short_url_sequence";
}
