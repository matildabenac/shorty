package com.assecosee.shorty.helpers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.URL;
import java.util.Random;

@Slf4j
@Component
public class UrlHelper {
    private final Random randomNumberGenerator = new Random();

    private final String hostName;
    private final String hostPort;

    public UrlHelper(@Value("${server.name}") String hostName, @Value("${server.port}") String hostPort) {
        this.hostName = hostName;
        this.hostPort = hostPort;
    }

    public String shortUrlGenerator(int len){
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'

        return randomNumberGenerator.ints(leftLimit, rightLimit + 1)
                .limit(len)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    public boolean isUrlValid(String url)
    {
        try {
            final URI uri = new URL(url).toURI();
            log.debug(uri.toString());
            return true;
        } catch (Exception e) {
            log.error(e.toString());
            e.printStackTrace();
            return false;
        }
    }

    public String getFullShortUrl(String shortUrl){
        final String url = "http://" + hostName + ":" + hostPort + "/short/" + shortUrl;
        log.debug(url);
        return url;
    }
}
