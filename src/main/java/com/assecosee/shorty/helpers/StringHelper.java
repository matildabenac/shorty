package com.assecosee.shorty.helpers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Random;

@Slf4j
@Component
public class StringHelper {

    private final Random randomNumberGenerator = new Random();

    public String  randomStringGenerator(int len){
        final int leftLimit = 42; // symbol '*'
        final int rightLimit = 122; // letter 'z'

        return randomNumberGenerator.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 90 || i >= 97))
                .limit(len)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    public boolean checkStringValidity(String string){
        return string.matches("[*-Za-z]+");
    }
}
