package com.assecosee.shorty.dtos.user;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UserDTO {

    @NotNull(message = "{accountId.notnull}")
    @Size(max = 255, message = "{accountId.size}")
    private String accountId;

    @NotNull(message = "{password.notnull}")
    @Size(max = 255, message = "{password.size}")
    private String password;

    public UserDTO(String accountId, String password) {
        this.accountId = accountId;
        this.password = password;
    }

}
