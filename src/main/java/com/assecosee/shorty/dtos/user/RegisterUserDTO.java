package com.assecosee.shorty.dtos.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class RegisterUserDTO {

    @NotNull(message = "{accountId.notnull}")
    @Size(max = 255, message = "{accountId.size}")
    @JsonProperty("accountId")
    private String accountId;
}
