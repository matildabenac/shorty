package com.assecosee.shorty.dtos.user;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UpdateUserPasswordDTO {

    @NotNull(message = "{password.notnull}")
    @Size(max = 255, message = "{password.size}")
    private String oldPassword;

    @NotNull(message = "{password.notnull}")
    @Size(max = 255, message = "{password.size}")
    private String newPassword;
}
