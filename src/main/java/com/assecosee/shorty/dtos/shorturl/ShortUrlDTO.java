package com.assecosee.shorty.dtos.shorturl;

import com.assecosee.shorty.helpers.UrlHelper;
import com.assecosee.shorty.models.ShortUrl;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ShortUrlDTO {

    @NotNull(message = "{originalUrl.notnull}")
    @Size(max = 255, message = "{originalUrl.size}")
    private String url;

    @NotNull(message = "{redirectType.notnull}")
    private int redirectType;

    private final UrlHelper urlHelper = new UrlHelper("localhost", "8080");

    public ShortUrlDTO() { }

    public ShortUrlDTO(ShortUrl shortUrl){
        this.url = urlHelper.getFullShortUrl(shortUrl.getShortenedUrl());
        this.redirectType = shortUrl.getRedirectType();
    }
}
