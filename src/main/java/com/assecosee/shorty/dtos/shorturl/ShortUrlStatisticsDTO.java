package com.assecosee.shorty.dtos.shorturl;

import com.assecosee.shorty.helpers.UrlHelper;
import com.assecosee.shorty.models.ShortUrl;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ShortUrlStatisticsDTO {

    @NotNull(message = "{originalUrl.notnull}")
    @Size(max = 255, message = "{originalUrl.size}")
    private String originalUrl;

    @NotNull(message = "{shortenedUrl.notnull}")
    @Size(max = 255, message = "{shortenedUrl.size}")
    private String shortenedUrl;

    @NotNull(message = "{redirectType.notnull}")
    private int redirectType;

    private int numberOfRedirects;

    private final UrlHelper urlHelper = new UrlHelper("localhost", "8080");

    public ShortUrlStatisticsDTO(ShortUrl shortUrl) {
        this.originalUrl = shortUrl.getOriginalUrl();
        this.shortenedUrl = urlHelper.getFullShortUrl(shortUrl.getShortenedUrl());
        this.redirectType = shortUrl.getRedirectType();
        this.numberOfRedirects = shortUrl.getNumberOfRedirects();
    }
}
