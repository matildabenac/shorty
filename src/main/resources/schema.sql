DROP TABLE IF EXISTS users;

CREATE TABLE users (
    id INT AUTO_INCREMENT  PRIMARY KEY,
    accountId VARCHAR(250) NOT NULL UNIQUE,
    password VARCHAR(250) NOT NULL
);

CREATE TABLE short_urls (
    id INT AUTO_INCREMENT PRIMARY KEY,
    originalUrl VARCHAR(250) NOT NULL UNIQUE,
    shortenedUrl VARCHAR(250) NOT NULL UNIQUE,
    redirectType INT,
    numberOfRedirects INT DEFAULT 0,
    userId INT
);